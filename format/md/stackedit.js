/**
 * Created by Alexander Nuikin (nukisman@gmail.com) on 24.06.15.
 */

var JoiDoc = require('../../')
var _ = require('lodash')

module.exports = {
    format: function (rows) {
        var self = this
        var lines = rows.map(function (row) {
            return self.formatTableRow(row)
        })
        var head = ['Path', 'Type', 'Presence', 'Description']
        var headSeparator = [':--', ':--', ':--', ':--']
        lines.unshift(
            self.formatLine(head),
            self.formatLine(headSeparator)
        )
        return lines.join('\n')
    },
    formatTableRow: function (data) {
        var self = this
        var cells = [
            self.formatPath(data.path),
            data.type,
            self.formatPresence(data.presence),
            self.formatExtDescr(data)
        ]
        return self.formatLine(cells)
    },
    formatExtDescr: function (data) {
        var descr = []
        var description = data.description.replace('\n', ' ').trim()
        if (description) {
            descr.push(description)
        }
        var ext = data.extension
        if (!_.isEmpty(ext)) {
            if (!_.isEmpty(descr)) {
                descr.push('')
            }
            for (var key in ext) {
                var value = ext[key]
                if (key === 'examples') {
                    var strs = []
                    for (var i in value) {
                        strs.push('`' + JoiDoc.jsStringify(value[i]) + '`')
                    }
                    value = strs.join('<br/>')
                }
                descr.push('**' + _.capitalize(key) + '**' + (value ? ': ' + value : ''))
            }
        }
        return descr.join('<br/>')
    },
    formatLine: function (cells) {
        return '| ' + cells.join(' | ') + ' |'
    },
    formatPresence: function (presence) {
        presence = presence || 'optional'
        switch (presence) {
            case 'optional':
                return 'Optional'
            case 'required':
                return '**Required**'
            case 'forbidden':
                return '***Forbidden***'
        }
    },
    formatPath: function (path) {
        if (_.isEmpty(path)) {
            return '<empty>'
        } else {
            var str = path[0]
            for (var i = 1; i < path.length; i++) {
                var key = path[i]
                if (_.includes('0123456789', key[0]) || _.includes(key, ' ')) {
                    str += '[' + key + ']'
                } else {
                    str += '.' + key
                }
            }
            return str
        }
    },
    formatJoiFunc: function (schema, singleObjArg, joiDoc) {
        var self = this
        joiDoc = joiDoc || JoiDoc
        if (schema._type !== 'func') {
            throw new Error('Not schema of func')
        } else {
            var meta = _.first(schema._meta, function (metaItem) {
                return metaItem.args instanceof Array
                    || metaItem.return._isJoi == true
            })
            var lines = [schema._description]
            if (meta) {
                var argRows = []
                if (meta.args) {
                    if (singleObjArg
                        && meta.args.length == 1
                        && meta.args[0]._type == 'object') {
                        lines.push('Parameters:')
                        var argsSchema = meta.args[0]
                        var children = argsSchema._inner.children
                        for (var i in children) {
                            var childPath = []
                            childPath.push(children[i].key)
                            joiDoc.visit(children[i].schema, argRows, childPath)
                        }
                        lines.push(self.format(argRows))
                        self.formatMultilineExamples(argsSchema._examples, lines)
                    } else {
                        lines.push('Arguments:')
                        for (var i in meta.args) {
                            joiDoc.visit(meta.args[i], argRows, [i])
                        }
                        lines.push(self.format(argRows))
                    }
                }
                if (meta.return) {
                    lines.push('Returns:')
                    switch (meta.return._type) {
                        case 'object':
                        case 'array':
                        case 'func':
                            var returnRows = []
                            joiDoc.visit(meta.return, returnRows, [])
                            lines.push(self.format(returnRows))
                            break
                        default:
                            lines.push(self.formatWoPath(meta.return, joiDoc))
                    }
                    self.formatMultilineExamples(meta.return._examples, lines)
                }
            }
            return lines.join('\n\n')
        }
    },
    formatMultilineExamples: function (examples, lines) {
        for (var i in examples) {
            lines.push('Example ' + (parseInt(i) + 1)
            + ':\n```\n'
            + JoiDoc.jsStringify(examples[i], 2)
            + '\n```')
        }
    },
    formatWoPath: function (schema, joiDoc) {
        var self = this
        joiDoc = joiDoc || JoiDoc
        var head = ['Type', 'Presence', 'Description']
        var headSeparator = [':--', ':--', ':--']
        var lines = [
            self.formatLine(head),
            self.formatLine(headSeparator)
        ]

        var rows = []
        joiDoc.visit(schema, rows, [])
        var data = rows[0]
        var cells = [
            data.type,
            self.formatPresence(data.presence),
            self.formatExtDescr(data)
        ]
        lines.push(self.formatLine(cells))

        return lines.join('\n')
    }
}