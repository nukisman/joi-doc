/**
 * Created by Alexander Nuikin (nukisman@gmail.com) on 10.06.15.
 */

var _ = require('lodash')
var stringify = require('json-stringify-safe')

module.exports = {
    visit: function (schema, rows, path) {
        var self = this
        path = path || []

        rows.push({
            path: path,
            type: schema._type,
            presence: schema._flags.presence || 'optional',
            description: schema._description || '',
            extension: self.makeExtension(schema)
        })

        switch (schema._type) {
            case 'object':
                var children = schema._inner.children
                for (var i in children) {
                    var childPath = _.clone(path)
                    childPath.push(children[i].key)
                    self.visit(children[i].schema, rows, childPath)
                }
                break
            case 'array':
                var items = schema._inner.items
                for (var i in items) {
                    var childPath = _.clone(path)
                    childPath.push(i)
                    self.visit(items[i], rows, childPath)
                }
                break
            case 'func':
                var meta = _.first(schema._meta, function(metaItem) {
                    return metaItem.args instanceof Array
                        || metaItem.return._isJoi == true
                })
                var fArgs = meta.args
                if (fArgs instanceof Array) {
                    var fArgsChildPath = _.clone(path)
                    fArgsChildPath.push('args')
                    for (var i in fArgs) {
                        var childPath = _.clone(fArgsChildPath)
                        childPath.push(i)
                        self.visit(fArgs[i], rows, childPath)
                    }
                }
                if (meta.return) {
                    var childPath = _.clone(path)
                    childPath.push('return')
                    self.visit(meta.return, rows, childPath)
                }
                break
            default:
            // do nothing
        }
    },
    makeExtension: function (schema) {
        var self = this
        var ext = {}
        switch (schema._type) {
            default:
                var tests = schema._tests
                for (var i in tests) {
                    ext[tests[i].name] = tests[i].arg
                }
                if (schema._unit) {
                    ext.unit = schema._unit
                }
                if (schema._flags.default) {
                    ext.default = schema._flags.default
                }
                if (schema._valids
                    && !_.isEmpty(schema._valids._set)) {
                    var key = schema._flags.allowOnly ? 'validOnly' : 'valid'
                    ext[key] = schema._valids._set.map(function (v) {
                        return self.jsStringify(v)
                    }).join(', ')
                }
                if (schema._invalids
                    && !_.isEmpty(schema._invalids._set)
                    && !_.isEqual(schema._invalids._set, [Infinity, -Infinity])
                    && !(_.isEqual(schema._invalids._set, ['']) && schema._flags.allowOnly)) { // TODO: avoid duplication
                    ext.invalid = schema._invalids._set.map(function (v) {
                        return self.jsStringify(v)
                    }).join(', ')
                }
                if (schema._examples && !_.isEmpty(schema._examples)) {
                    ext.examples = schema._examples
                }
        }
        return ext
    },
    jsStringify: function (v, indent) { // TODO: make recursive
        switch (v) {
            case Infinity:
                return 'Infinity'
            case -Infinity:
                return '-Infinity'
            case NaN:
                return 'NaN'
            default:
                return stringify(v, null, indent)
        }
    }
}